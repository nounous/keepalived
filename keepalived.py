#!/bin/env python3
import argparse
import json
import os
import subprocess
import sys
import time
from enum import Enum

path = os.path.dirname(os.path.abspath(__file__))

class Type(Enum):
	GROUP = 'GROUP'
	INSTANCE = 'INSTANCE'

	def __str__(self):
		return self.value

class State(Enum):
	MASTER = 'MASTER'
	BACKUP = 'BACKUP'
	FAULT = 'FAULT'
	STOP = 'STOP'
	DELETED = 'DELETED'

	def __str__(self):
		return self.value

def handling(state):
	if state == State.MASTER:
		return 'start'
	else:
		return 'stop'

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Start and stop service on keepalived switch')
	parser.add_argument('TYPE', type=Type, choices=list(Type))
	parser.add_argument('NAME', type=str)
	parser.add_argument('STATE', type=State, choices=list(State))
	parser.add_argument('PRIORITY', type=int)
	args = parser.parse_args()

	if args.STATE == State.MASTER:
		time.sleep(2)

	with open(os.path.join(path, "keepalived.json")) as config_file:
		config = json.load(config_file)

	for service in config['services'][args.NAME]:
	    subprocess.run(['systemctl', handling(args.STATE), service])
